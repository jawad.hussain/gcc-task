# GCC Task  
  
## Discription:  
A calculator library is created by the combination of 4 seprate files namely:  
- add.cpp  
- sub.cpp  
- multiply.cpp  
- divide.cpp  

The calculator library is called **lib_calc.a**  
  
## How to Run:  
To create executeable files a `Makefile` is made. Just run the following commands according to need:  
  
- To make complete code run `make`
- To make only main file run `make m`
- To make only library *lib_calc* `make lib`

Afterwards run the command `./main`

## Cleaning Extra Files:
To clean extra files we can use the `Makefile` again:

- To clean all made files run `make clean`
- To clean everything made execpt the executable run `make clean_o`
