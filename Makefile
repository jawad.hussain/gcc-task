#---------- if you want to make everything ----------
main:
		g++ -c -Wall add.cpp sub.cpp multiply.cpp divide.cpp
		ar rcs lib_calc.a add.o sub.o multiply.o divide.o
		g++ -c main.cpp -o main.o
		g++ -o main main.cpp -L. -l_calc
#---------- if you want to make only the main ---------- 
m:
		g++ -c main.cpp -o main.o
		g++ -o main main.cpp -L. -l_calc
#---------- if you want to make only the libaray ----------
lib:
		g++ -c -Wall add.cpp sub.cpp multiply.cpp divide.cpp
		ar rcs lib_calc.a add.o sub.o multiply.o divide.o 
#---------- To clean only .o files ----------
clean_o:
		rm -f main.o add.o divide.o sub.o multiply.o
#---------- To clean all extra files ----------
clean:
		rm -f main main.o add.o divide.o sub.o multiply.o
