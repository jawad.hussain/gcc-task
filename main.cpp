#include <iostream>
#include "calculator.h"
using namespace std;

int main()
{
    calc obj1;
    obj1.get();
    cout << "add:      " <<  obj1.add() << endl;
    cout << "sub:      " <<  obj1.subtract() << endl;
    cout << "divide:   " <<  obj1.divide() << endl;
    cout << "multiply: " <<  obj1.multiply() << endl;
}